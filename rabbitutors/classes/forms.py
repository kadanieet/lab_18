from classes.models import Lesson, ClientRating, StudentRating
from django import forms
from django.conf import settings


class AddLessonForm(forms.ModelForm):
    date = forms.DateField(required=True)
    start = forms.TimeField(required=True)

    class Meta:
        model = Lesson
        exclude = ('assigned', 'lesson_id')


class ClientRatingForm(forms.ModelForm):
    time_keeping = forms.MultipleChoiceField(label='Timekeeping', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    dressing = forms.MultipleChoiceField(label='Dressing', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    communication = forms.MultipleChoiceField(label='Communication', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    class_attendance = forms.MultipleChoiceField(label='Class Attendance', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    giving_homework = forms.MultipleChoiceField(label='Giving of Home Work', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    homework_evaluation = forms.MultipleChoiceField(label='Home Work Evaluation', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)

    class Meta:
        model = ClientRating
        fields = ['time_keeping', 'dressing', 'communication', 'class_attendance', 'giving_homework', 'homework_evaluation',]


class StudentRatingForm(forms.ModelForm):
    friendliness = forms.MultipleChoiceField(label='Friendliness', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    mentorship_advice = forms.MultipleChoiceField(label='Mentorship & Advice', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    subject_mastery = forms.MultipleChoiceField(label='Subject Mastery', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)
    teaching = forms.MultipleChoiceField(label='Teaching', widget=forms.RadioSelect, choices=settings.RATING_CHOICES)

    class Meta:
        model = StudentRating
        fields = ['friendliness','mentorship_advice', 'subject_mastery', 'teaching',]
