# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-22 14:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0004_auto_20180522_1718'),
        ('tutors', '0009_auto_20180522_1718'),
        ('classes', '0004_auto_20180522_0009'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientRating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_keeping', models.PositiveSmallIntegerField(default=0, verbose_name='Time Keeping')),
                ('dressing', models.PositiveSmallIntegerField(default=0, verbose_name='Dressing')),
                ('communication', models.PositiveSmallIntegerField(default=0, verbose_name='Communication')),
                ('class_attendance', models.PositiveSmallIntegerField(default=0, verbose_name='Class Attendance')),
                ('giving_homework', models.PositiveSmallIntegerField(default=0, verbose_name='Giving of Home Work')),
                ('homework_evaluation', models.PositiveSmallIntegerField(default=0, verbose_name='Home Work Evaluation')),
                ('client', models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='clients.Client')),
                ('tutor', models.OneToOneField(default=None, on_delete=django.db.models.deletion.CASCADE, to='tutors.Tutor')),
            ],
        ),
        migrations.CreateModel(
            name='StudentRating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('friendliness', models.PositiveSmallIntegerField(default=0, verbose_name='Friendliness')),
                ('mentorship_advice', models.PositiveSmallIntegerField(default=0, verbose_name='Mentorship & Advice')),
                ('subject_mastery', models.PositiveSmallIntegerField(default=0, verbose_name='Subject Mastery')),
                ('teaching', models.PositiveSmallIntegerField(default=0, verbose_name='Teaching')),
                ('assigned', models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to='classes.Assigned')),
            ],
        ),
    ]
