# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-23 06:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0016_student_has_lesson_times'),
        ('tutors', '0009_auto_20180522_1718'),
        ('classes', '0006_auto_20180523_0822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assigned',
            name='student',
        ),
        migrations.AddField(
            model_name='assigned',
            name='student',
            field=models.ManyToManyField(default=1, to='students.Student'),
        ),
        migrations.RemoveField(
            model_name='assigned',
            name='tutor',
        ),
        migrations.AddField(
            model_name='assigned',
            name='tutor',
            field=models.ManyToManyField(default=None, to='tutors.Tutor'),
        ),
    ]
