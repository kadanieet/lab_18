# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from clients.models import Client
from django.db import models
from tutors.models import Tutor
from students.models import Student


class Assigned(models.Model):
    ass_id = models.AutoField(primary_key=True, null=False)
    tutor = models.CharField(Tutor, max_length=4, default=None, null=False)
    student = models.PositiveSmallIntegerField(Student, default=1, null=False)
    subject = models.CharField(max_length=3, verbose_name='Subject', default=None)
    date = models.DateField(verbose_name='Date', null=False, auto_now=True)
    time = models.TimeField(verbose_name='Time', null=False, auto_now=True)
    rated = models.BooleanField(default=False)
    rating = models.SmallIntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Assignments'


class Lesson(models.Model):
    lesson_id = models.AutoField(primary_key=True, null=False)
    assigned = models.OneToOneField(Assigned, default=1, on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=3, verbose_name='Subject', default=None)
    topic = models.CharField(max_length=15, verbose_name='Topic Covered', default=None)
    date = models.DateField(verbose_name='Date', auto_now=True)
    start = models.TimeField(verbose_name='Start Time', default=None)
    end = models.TimeField(verbose_name='End Time', auto_now=True)


class Progress(models.Model):
    assigned = models.OneToOneField(Assigned, default=1, on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=3, verbose_name='Subject', default=None)
    cat1 = models.SmallIntegerField(verbose_name='CAT 1 Score', default=None)
    cat2 = models.SmallIntegerField(verbose_name='CAT 2 Score', default=None)
    exam = models.SmallIntegerField(verbose_name='Exam Score', default=None)

    class Meta:
        verbose_name_plural = 'Progresses'


class ClientRating(models.Model):
    tutor = models.OneToOneField(Tutor, default=None, on_delete=models.CASCADE, null=False)
    client = models.OneToOneField(Client, default=None, on_delete=models.CASCADE, null=False)
    time_keeping = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Time Keeping')
    dressing = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Dressing')
    communication = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Communication')
    class_attendance = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Class Attendance')
    giving_homework = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Giving of Home Work')
    homework_evaluation = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Home Work Evaluation')


class StudentRating(models.Model):
    assigned = models.OneToOneField(Assigned, default=1, on_delete=models.CASCADE, null=False)
    friendliness = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Friendliness')
    mentorship_advice = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Mentorship & Advice')
    subject_mastery = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Subject Mastery')
    teaching = models.PositiveSmallIntegerField(default=0, null=False, verbose_name='Teaching')