from django.conf.urls import url
from . import views


app_name = 'classes'


urlpatterns = [
    url(r'^add-lesson/$', views.add_lesson, name='add_lesson'),
    url(r'^rate-tutor/$', views.rate_tutor, name='rate_tutor'),
]