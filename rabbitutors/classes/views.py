# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .forms import AddLessonForm, ClientRatingForm, StudentRatingForm


def add_lesson(request):
    context_dict = {}

    if request.method == 'POST':
        add_lesson_form = AddLessonForm(data=request.POST)
        if add_lesson_form.is_valid():
            pass
        else:
            print(add_lesson_form.errors)
    else:
        add_lesson_form = AddLessonForm()

    context_dict['add_lesson_form'] = add_lesson_form

    return render(request, 'tutors/add_lesson.html', context_dict)


def rate_tutor(request):
    context_dict = {}
    if request.method == 'POST':
        client_rate_form = ClientRatingForm(data=request.POST)
    else:
        client_rate_form = ClientRatingForm()

    context_dict['client_rate_form'] = client_rate_form

    return render(request, 'clients/rate_tutor.html', context_dict)
