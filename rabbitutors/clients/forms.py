from django import forms
from clients.models import Client


class BioForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('Surname', 'Middle_Name', 'First_Name', 'ID_Number', 'Phone_Number')


class ProSocialForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('Occupation', 'Organization', 'Religion', 'Gender', 'Profile_Picture')


class EditUserForm(forms.ModelForm):
    class Meta:
        model = Client
        exclude = ('Client_ID', 'longitude', 'latitude', 'user', 'Profile_Picture')


class EditProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)

        profile = kwargs.get('instance')
        if profile:
            kwargs['instance'] = profile

        self.user_form = EditUserForm(*args, **kwargs)

        self.fields.update(self.user_form.fields)
        self.initial.update(self.user_form.initial)

    class Meta:
        model = Client
        exclude = ('Client_ID', 'longitude', 'latitude','user', 'Profile_Picture')