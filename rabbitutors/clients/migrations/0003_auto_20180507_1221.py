# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-07 09:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0002_client_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='Gender',
            field=models.CharField(blank=True, choices=[(b'O', b'Other'), (b'F', b'Female'), (b'M', b'Male')], default=None, max_length=1),
        ),
        migrations.AlterField(
            model_name='client',
            name='Religion',
            field=models.CharField(blank=True, choices=[(b'r3', b'Muslim'), (b'r1', b'Christian'), (b'r2', b'Hindu'), (b'r4', b'Pagan')], default=None, max_length=2),
        ),
    ]
