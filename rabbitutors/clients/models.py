# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings


class Client(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default='')
    client_id = models.CharField(name='Client_ID',max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField(name='ID_Number', default=None)
    surname = models.CharField(name='Surname', max_length=10)
    middle_name = models.CharField(name='Middle_Name', max_length=10, null=True)
    first_name = models.CharField(name='First_Name', max_length=10)
    gender = models.CharField(name='Gender', max_length=1, choices=settings.GENDER_CHOICES, default=None, blank=True)
    religion = models.CharField(name='Religion', max_length=2, choices=settings.RELIGION_CHOICES, default=None, blank=True)
    occupation = models.CharField(name='Occupation', max_length=20, null=True)
    organization = models.CharField(name='Organization', max_length=30, null=True, help_text='Enter the organization or institution in which you practice your occupation')
    phone = models.CharField(name='Phone_Number', max_length=10)
    longitude = models.FloatField()
    latitude = models.FloatField()
    picture = models.ImageField(upload_to='profile_images', blank=True, null=True, name='Profile_Picture')


