from django.conf.urls import url, include
from clients import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'clients'

urlpatterns = [
    url(r'^payment/$', views.payment, name='payment'),
    url(r'^portal/$', views.portal, name='portal'),
    url(r'^profile/view/$', views.profile, name='viewprofile'),
    url(r'^registered-students/$', views.students, name='students'),
    url(r'^bio-data/$', views.biodata, name='biodata'),
    url(r'^location/$', views.location, name='location'),
    url(r'^pro-social-data/$', views.prosocialdata, name='prosocialdata'),
    url(r'^profile/edit/$', views.edit_profile, name='editprofile'),
    url(r'^tutor-rating/$', views.rating, name='rating'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)