# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from clients.forms import BioForm, ProSocialForm, EditProfileForm
from rabbitutors.utils import save_data_to_model, set_profile, clear_session_data, clear_unicode_list, clear_unicode_string
from .models import Client
from students.models import Student
from django.conf import settings
from tutors.models import Tutor
from classes.models import Assigned


def client_check(user):
	return user.is_client


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def portal(request):
    context_dict = {}
    request.session['client_id'] = 'C' + str(request.user.id)

    if request.user.has_profile:
        context_dict['profile'] = True
        if not Student.objects.filter(client_id=request.session['client_id']).exists():
            context_dict['students'] = False
        else:
            context_dict['students'] = True
            students_list = Student.objects.filter(client_id=request.session['client_id']).all()
            no_subjects = []
            no_lesson_times = []
            for student in students_list:
                if not student.has_subjects:
                    no_subjects.append(student)

                if not student.has_lesson_times:
                    no_lesson_times.append(student)

            context_dict['no_subjects'] = no_subjects
            context_dict['no_lesson_times'] = no_lesson_times
    else:
        context_dict['profile'] = False

    return render(request, 'clients/portal.html', context_dict)


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def profile(request):
    context_dict = {}

    if request.user.has_profile:
        context_dict['profile'] = True

        client_profile = Client.objects.get(Client_ID=request.session['client_id'])

        context_dict['name'] = client_profile.First_Name + ' ' + client_profile.Middle_Name + ' ' + client_profile.Surname
        context_dict['idno'] = client_profile.ID_Number
        context_dict['gender'] = settings.GENDERS[client_profile.Gender]
        context_dict['occ'] = client_profile.Occupation
        context_dict['org'] = client_profile.Organization
        context_dict['phone'] = client_profile.Phone_Number
        context_dict['relg'] = settings.RELIGIONS[client_profile.Religion]
        context_dict['email'] = request.user.email
    else:
        context_dict['profile'] = False

    return render(request, 'clients/profile_view.html', context_dict)


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def edit_profile(request):
    if request.method == 'POST':
        edit_profile_form = EditProfileForm(data=request.POST, files=request.FILES, instance=Client.objects.get(Client_ID=request.session['client_id']))
        if edit_profile_form.has_changed():
            if edit_profile_form.is_valid():
                data_dict = {}

                for field in clear_unicode_list(edit_profile_form.changed_data):
                    data_dict[clear_unicode_string(field)] = clear_unicode_string(edit_profile_form.cleaned_data[clear_unicode_string(field)])

                Client.objects.update_or_create(Client_ID=request.session.get('client_id'), defaults=data_dict)
                request.session['updated'] = data_dict.keys()
                return redirect('clients:viewprofile')
            else:
                print(edit_profile_form.errors)
        else:
            return redirect('clients:viewprofile')
    else:
        edit_profile_form = EditProfileForm(instance=Client.objects.get(Client_ID=request.session['client_id']))

    return render(request, 'clients/profile_edit.html', {'edit_profile_form': edit_profile_form})


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def students(request):
    context_dict = {}
    if Student.objects.filter(client_id=request.session['client_id']).exists():
        students_list = Student.objects.filter(client_id=request.session['client_id']).all()
        context_dict['students'] = students_list
    else:
        context_dict['students'] = None

    return render(request, 'clients/students.html', context_dict)


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def rating(request):
    # Dictionaries
    context_dict = {}
    tutor_student_dict = {}

    # String Lists
    student_ids = []
    assigned_students_ids = []
    tutor_ids = []

    # Object Lists
    assigned_students = []
    assigned_tutors = []

    if Student.objects.filter(client_id=request.session['client_id']).exists():  # Check if the client has registered students
        for each_student in Student.objects.filter(client_id=request.session['client_id']).all():
            student_ids.append(each_student.student_id)  # fetch all the registered students

        for each_student_id in student_ids:
            if Assigned.objects.filter(student=each_student_id, rated=False).exists():  # Check if the all tutors have been rated
                for each_assignment in Assigned.objects.filter(student=each_student_id, rated=False).all():
                    assigned_students_ids.append(each_assignment.student)  # Fetch the list of all students with unrated tutors
                    tutor_ids.append(each_assignment.tutor)  # Fetch the list of all unrated tutors

                    # Clear the unicode character
                    tutor_ids = clear_unicode_list(tutor_ids)

                    if tutor_student_dict.keys():  # check if the tutor/students dict is empty
                        for tutor_id in tutor_student_dict.keys():
                            if tutor_id == each_assignment.tutor: # Check if tutor is already in a relation in the dict
                                for student in tutor_student_dict[tutor_id].keys():
                                    if student == each_assignment.student: # Check if student is already in the relation
                                        tutor_student_dict[tutor_id][student].append(clear_unicode_string(each_assignment.subject)) # append the subject to the student's subject list
                                    else: # Create a new relation for the student
                                        tutor_student_dict[tutor_id][each_assignment.student] = [clear_unicode_string(each_assignment.subject)] # Create a new list of subjects for the student
                            else: # Create a new relation for the tutor not in the dict
                                tutor_student_dict[clear_unicode_string(each_assignment.tutor)] = {
                                    each_assignment.student: [clear_unicode_string(each_assignment.subject)]}
                    else:  # Initialize the tutor/students dict with a relation
                        tutor_student_dict[clear_unicode_string(each_assignment.tutor)] = {
                            each_assignment.student: [clear_unicode_string(each_assignment.subject)]}
            else:
                context_dict['unrated_tutors'] = None
    else:
        context_dict['students'] = None

    # Remove duplicates from the students and tutors lists
    assigned_students_ids = list(set(assigned_students_ids))
    tutor_ids = list(set(tutor_ids))

    # Fetch the student and the tutors profiles
    for each_tutor_id in tutor_ids:
        assigned_tutors.append(Tutor.objects.get(Tutor_ID=each_tutor_id))

    for each_student_id in assigned_students_ids:
        assigned_students.append(Student.objects.get(student_id=each_student_id))

    context_dict['assigned_tutors'] = assigned_tutors
    context_dict['assigned_students'] = assigned_students
    context_dict['tutor_student'] = tutor_student_dict

    print(assigned_students)
    print(assigned_tutors)
    print(tutor_student_dict)

    return render(request, 'clients/tutor_rating.html', context_dict)


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def payment(request):
    return render(request, 'clients/payment.html')


"""------------------------------------------REGISTRATION VIEWS---------------------------------------"""


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def biodata(request):
	if request.method == 'POST':
		biodataform = BioForm(data=request.POST)
		if biodataform.is_valid():
			request.session['c_biodata'] = biodataform.cleaned_data
			return redirect('clients:location')
		else:
			print(biodataform.errors)
	else:
		biodataform = BioForm()

	return render(request, 'registration/bio_data.html', {'biodataform': biodataform})


@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def location(request):
	if request.method == 'POST':
		long = request.POST.get('long')
		lat = request.POST.get('lat')
		# We need to check the credibility of the values before submit
		if (lat > -90 or lat < 90) and (long > -90 or long < 90):
			bio_data = request.session.get('c_biodata')
			bio_data.update({'latitude':lat, 'longitude':long})
			request.session['c_biodata'] = bio_data
			return  redirect('clients:prosocialdata')

        return render(request, 'registration/location.html')

@login_required
@user_passes_test(client_check, login_url=settings.RESTRICTED_URL)
def prosocialdata(request):
    if request.method == 'POST':
        prosocialform = ProSocialForm(data=request.POST, files=request.FILES)
        if prosocialform.is_valid():
            #prosocial_data = prosocialform.cleaned_data
            bio_data = request.session.get('c_biodata')
            bio_data.update(prosocialform.cleaned_data)
            bio_data.update({'Client_ID': request.session['client_id'], 'user_id': request.user.id})
            print(bio_data)
            if save_data_to_model(Client, bio_data):
                if set_profile(request):
                    if clear_session_data(request, 'c_biodata'):
                        return redirect('clients:portal')
        else:
            print(prosocialform.errors)
    else:
        prosocialform = ProSocialForm()

    return render(request, 'registration/pro_social_data.html', {'prosocialform': prosocialform})