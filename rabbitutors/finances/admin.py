# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from finances.models import Account, Payments

# Register your models here.
admin.site.register(Account)
admin.site.register(Payments)
