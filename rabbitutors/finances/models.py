# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from classes.models import Assigned, Lesson
from tutors.models import Tutor
from clients.models import Client
from django.conf import settings


class Account(models.Model):
    account_id = models.CharField(max_length=6, name='Account_ID')
    amount = models.SmallIntegerField(name='Amount', null=False)


class Payments(models.Model):
    payment_id = models.CharField(max_length=12, name='Payment_ID', null=False)
    user = models.CharField(max_length=4, null=False, default='null')
    amount = models.SmallIntegerField(name='Amount', null=False)

    class Meta:
        verbose_name_plural = 'Payments'
