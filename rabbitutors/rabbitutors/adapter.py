from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth import logout
from django.shortcuts import redirect


class AccountAdapter(DefaultAccountAdapter):

    def save_user(self, request, user, form, commit=True):
        data = form.cleaned_data
        if request.POST.get('next') == '/tutor/portal/':
            user.is_tutor = True
        elif request.POST.get('next') == '/client/portal/':
             user.is_client = True
        else:
            return redirect('homepage')

        user.email = data['email']

        if 'password1' in data:
            user.set_password(data['password1'])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            user.save()
        return user


    def get_login_redirect_url(self, request):
        if request.user.is_client:
            path = "/client/portal/"
        elif request.user.is_tutor:
            path = "/tutor/portal/"
        elif request.user.is_superuser:
            logout(request)
        return path

    