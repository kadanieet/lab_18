# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from rabbitutors.models import RabbiUser

class RabbiUserAdmin(admin.ModelAdmin):
    pass

admin.site.register(RabbiUser, RabbiUserAdmin)