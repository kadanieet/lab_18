from .models import PrimarySubjects, SecondarySubjects
from django import forms
from django.conf import settings
from .models import RabbiUser


class SubjectsForm(forms.ModelForm):
    class Meta:
        model = SecondarySubjects
        fields = '__all__'


class PriSubjectsForm(forms.ModelForm):
    class Meta:
        model = PrimarySubjects
        fields = '__all__'


class EditUserForm(forms.ModelForm):
    class Meta:
        model = RabbiUser
        fields = ('email',)