from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models


class RabbiUser(AbstractUser):
    is_client = models.BooleanField(default=False)
    is_tutor = models.BooleanField(default=False)
    has_profile = models.BooleanField(default=False)


class SecondarySubjects(models.Model):
    mathematics = models.BooleanField(default=False, name='Mathematics')
    english = models.BooleanField(default=False, name='English')
    kiswahili = models.BooleanField(default=False, name='Kiswahili')
    cre = models.BooleanField(default=False, name='CRE', verbose_name='Christian Religious Education')
    ire = models.BooleanField(default=False, name='IRE', verbose_name='Islam Religious Education')
    hre = models.BooleanField(default=False, name='HRE', verbose_name='Hindu Religious Education')
    french = models.BooleanField(default=False, name='French')
    german = models.BooleanField(default=False, name='German')
    spanish = models.BooleanField(default=False, name='Spanish')
    chemistry = models.BooleanField(default=False, name='Chemistry')
    biology = models.BooleanField(default=False, name='Biology')
    physics = models.BooleanField(default=False, name='Physics')
    agriculture = models.BooleanField(default=False, name='Agriculture')
    geography = models.BooleanField(default=False, name='Geography')
    history = models.BooleanField(default=False, name='History')
    business_studies = models.BooleanField(default=False, name='Business_Studies')
    music = models.BooleanField(default=False, name='Music')
    home_science = models.BooleanField(default=False, name='Home_Science')
    art_and_craft = models.BooleanField(default=False, name='Art_and_Craft')


class PrimarySubjects(models.Model):
    mathematics = models.BooleanField(default=False, name='Mathematics')
    english = models.BooleanField(default=False, name='English')
    kiswahili = models.BooleanField(default=False, name='Kiswahili')
    cre = models.BooleanField(default=False, name='CRE', verbose_name='Christian Religious Education')
    ire = models.BooleanField(default=False, name='IRE', verbose_name='Islam Religious Education')
    hre = models.BooleanField(default=False, name='HRE', verbose_name='Hindu Religious Education')
    science = models.BooleanField(default=False, name='Science')
    social_studies = models.BooleanField(default=False, name='Social_Studies')