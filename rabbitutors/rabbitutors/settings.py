import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Important directories

TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
STATIC_DIR = os.path.join(BASE_DIR, 'static')
MEDIA_DIR = os.path.join(BASE_DIR, 'media')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'se^3-pk16=yxe^=qqhkk$6td_z&-rtr233i4l*=_7$u7w&acqv'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rabbitutors',
    'clients',
    'tutors',
    'students',
    'classes',
    'finances',
    #'register',
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'bootstrap_toolkit', # Not used anyway
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'rabbitutors.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATE_DIR, ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'rabbitutors.wsgi.application'

AUTHENTICATION_BACKENDS = (
    # Needed to login by user name

    'django.contrib.auth.backends.ModelBackend',

    # authentication methods, login by email

    'allauth.account.auth_backends.AuthenticationBackend',
)

# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'rabbitutors',
        'USER' : 'root',
        'PASSWORD' : '',
        'HOST' : '127.0.0.1',
        'PORT' : '3306',
    }
}


# Password hashers

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
]

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': { 'min_length': 8, }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'rabbitutors.RabbiUser'
#AUTH_ACCOUNT_FORM_CLASSES = ['rabbitutors.forms.ClientSignUpForm',]

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

#media files

MEDIA_ROOT = MEDIA_DIR
MEDIA_URL = '/media/'


# Static files (CSS, JavaScript, Images)

STATICFILES_DIRS = [STATIC_DIR,]
STATIC_URL = '/static/'

# Authentication, Authorization and Registration

# Ensure SITE_ID is set sites app
SITE_ID = 1

# EMAIL_BACKEND so allauth can proceed to send confirmation emails
# ONLY for development/testing use console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Custom allauth settings
# Use email as the primary identifier
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True

ACCOUNT_ADAPTER = 'rabbitutors.adapter.AccountAdapter'
# Make email verification mandatory to avoid junk email accounts
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
# Eliminate need to provide username, as it's a very old practice
ACCOUNT_USERNAME_REQUIRED = False

ACCOUNT_LOGIN_ATTEMPTS_LIMIT = 5
# ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT (=300)
ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email'
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'username'

# Registration Package Config
REGISTRATION_OPEN = True

# ACCOUNT_ACTIVATION_DAYS = 1

REGISTRATION_AUTO_LOGIN = False

# LOGIN_REDIRECT_URL = '/portal/'

LOGIN_URL = '/accounts/login/'

# Restricted Areas

RESTRICTED_URL = '/restricted/'

# Modeling Stuff

GENDER_CHOICES = {('M', 'Male'), ('F','Female'), ('O','Other')}
RELIGION_CHOICES = {('r1', 'Christian'), ('r2', 'Hindu'), ('r3', 'Muslim'), ('r4', 'Pagan')}
GRADE_CHOICES = {('A','A'), ('A-','A-'), ('B+','B+'), ('B','B'), ('B-','B-'), ('C+','C+')}
LEVEL_CHOICES = {('P','Primary School'), ('S','Secondary/High School')}
CLASS_CHOICES = {('1','1'), ('2','2'), ('3','3'), ('4','4'), ('5','5'), ('6','6'), ('7','7'), ('8','8')}
FORM_CHOICES = {('1','1'), ('2','2'), ('3','3'), ('4','4')}
LESSON_TIMES = {('M', 'Morning: 7 am - 11 am'), ('A', 'Afternoon: 12 pm - 4 pm'), ('E', 'Evening: 5 pm - 9 pm ')}
RATING_CHOICES = {('1', 'Very Poor'), ('2', 'Poor'),('3', 'Average'), ('4', 'Good'),('5', 'Very Good')}
TEACHING_LEVEL_CHOICES = {('P', 'Primary School'), ('S', 'Secondary/High School'), ('A', 'Both Levels')}
# ('C','C'), ('C-','C-'), ('D+','D+'), ('D','D'), ('D-','D-'), ('E','E')


# views stuff

RELIGIONS = {'r1': 'Christian', 'r2': 'Hindu', 'r3': 'Muslim', 'r4': 'Pagan'}
GENDERS = {'M': 'Male', 'F':'Female', 'O':'Other'}
LEVELS = {'P':'Primary School', 'S':'Secondary/High School'}
LESSONS = {'M':'Morning: 7 am - 11 am', 'A':'Afternoon: 12 pm - 4 pm', 'E':'Evening: 5 pm - 9 pm'}
TEACHING_LEVELS = {'P':'Primary School', 'S':'Secondary/High School', 'A':'Both Levels'}


# Allocation stuff
MINIMUM_OVERALL_GRADE_PRIMARY = ['A', 'A-', 'B+', 'B', 'B-','C+']
MINIMUM_OVERALL_GRADE_SECONDARY = ['A', 'A-', 'B+']

MINIMUM_SUBJECT_GRADE_PRIMARY = MINIMUM_OVERALL_GRADE_PRIMARY
MINIMUM_SUBJECT_GRADE_SECONDARY = MINIMUM_OVERALL_GRADE_SECONDARY
