from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from rabbitutors import views
from django.conf import settings

app_name = 'rabbitutors'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.homepage, name='homepage'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^client/', include('clients.urls')),
    url(r'^tutor/', include('tutors.urls')),
    url(r'^classes/', include('classes.urls')),
    url(r'^students/', include('students.urls')),
    url(r'^payments/', include('finances.urls')),
    url(r'^restricted/$', views.restricted, name='restricted')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
