from tutors.models import Tutor, Grades
from students.models import Student
from django.conf import settings


def file_to_dict(text_file, delimiter='-'):
    data_dict = {}
    try:
        with open(text_file) as the_file:
            for each_line in the_file:
                clean_line = each_line.strip()
                (value, initial) = clean_line.split(delimiter, 1)
                key = initial.strip()
                value = value.strip()
                data_dict[key] = value

    except IOError as err:
        print ('File error: ' + str(err))

    return data_dict


def assign_tutor_to_student(student_id):
    student = Student.objects.get(student_id=student_id)

    """Step 1 - Filter Allocated and Not Allocated"""
    non_assigned_tutors = Tutor.objects.filter(students_assigned=0).all()
    assigned_tutors = Tutor.objects.exclude(students_assigned=0)

    """Step 2 - Filter on Overall Grade"""
    non_assigned_tutors_ids = []
    for tutor in non_assigned_tutors:
        non_assigned_tutors_ids.append(tutor.Tutor_ID)

    if student.Level == 'P': # C+ minimum
        check_overall_grade(non_assigned_tutors_ids, settings.MINIMUM_OVERALL_GRADE_PRIMARY)
    elif student.Level == 'S': # B+ minimum
        check_overall_grade(non_assigned_tutors_ids, settings.MINIMUM_OVERALL_GRADE_SECONDARY)

    """Step 3 - Filter on Subject Grade"""
    student_subjects = clear_unicode_string(student.Subjects).split(',', student.Subjects.count(',')).sort()
    for tutor in non_assigned_tutors_ids:
        tutor_subjects = clear_unicode_string(tutor.Subjects).split(',', tutor.Subjects.count(',')).sort()
        # Stage 3a
        # The tutor has more registered number of subjects than the student
        if len(student_subjects) < len(tutor_subjects):
            pass


def check_overall_grade(the_tutors_list, minimum_grades):
    for tutor_id in the_tutors_list:
        if clear_unicode_string(Grades.objects.get(tutor_id=tutor_id).avg_grade) not in minimum_grades:
            the_tutors_list.remove(tutor_id)


def save_data_to_model(Model, data_dict):
	model = Model.objects.create(**data_dict)
	model.save()
	return True


def set_profile(request):
    request.user.has_profile = True
    request.user.save()
    return True


def clear_session_data(request, keys):
    for key in keys:
        del request.session[key]
    return True


def create_fields_list(data):
    fields = []
    for key in data.keys():
        if data[key]:
            fields.append(key)
    return fields


def list_to_string(list):
    string = ''
    for item in list:
        item.encode('ascii', 'ignore')
        string = string + ',' + str(item)
    string = string[1:]
    return string


def clear_unicode_list(the_data):
    ascii_list = []
    for item in the_data:
        item.encode('ascii', 'ignore')
        ascii_list.append(item)
    return ascii_list


def clear_unicode_string(the_data):
    if type(the_data) == int:
        return the_data
    else:
        return the_data.encode('ascii', 'ignore')


def create_changed_data_dict(the_form):
    data_dict = {}
    for field in clear_unicode_list(the_form.changed_data):
        data_dict[clear_unicode_string(field)] = clear_unicode_string(
            the_form.cleaned_data[clear_unicode_string(field)])
    return data_dict


def replace_underscore(the_data):
    if type(the_data) == str:
        return the_data.replace('_',' ')
    elif type(the_data) == list:
        data = []
        for item in the_data:
            data.append(item.replace('_',' '))
        return data
