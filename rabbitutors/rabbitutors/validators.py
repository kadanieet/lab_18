from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_points(value):
    if value > 84 or value < 7:
        raise ValidationError (
            _('%(value)s is not a correct Points Score'),
            params={'value':value},
        )


def validate_scores(value):
    if value > 99 or value < 0:
        raise ValidationError(
            _('%(value)s is not a correct Score'),
            params={'value': value},
        )