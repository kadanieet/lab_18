from django.shortcuts import render


def homepage(request):
    return render(request, 'homepage.html')


def restricted(request):
    return render(request, 'restricted.html')