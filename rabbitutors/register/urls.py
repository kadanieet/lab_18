from django.conf.urls import url, include
from register import views

#app_name = 'tutors'

urlpatterns = [
    url(r'^biodata/$', views.biodata, name='biodata'),
    url(r'^location/$', views.location, name='location'),
    url(r'^prosocialdata/$', views.prosocialdata, name='prosocialdata'),
    url(r'^scoresdata/$', views.scoresdata, name='scoresdata'),
    url(r'^academicdata/$', views.academicdata, name='academicdata'),
]