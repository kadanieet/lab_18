# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from tutors.forms import BioForm, LocationForm, ScoresForm, ProSocialForm, AcademicsForm
from django.contrib.auth.decorators import login_required

@login_required
def biodata(request):
	if request.method == 'POST':
		biodataform = BioForm(data=request.POST)
	else:
		biodataform = BioForm()

	return render(request, 'registration/bio_data.html', {'biodataform': biodataform})


@login_required
def location(request):
	return render(request, 'registration/location.html')

@login_required
def prosocialdata(request):
	if request.method == 'POST':
		prosocialform = ProSocialForm(data=request.POST)
	else:
		prosocialform = ProSocialForm()

	return render(request, 'registration/pro_social_data.html', {'prosocialform': prosocialform})

@login_required
def scoresdata(request):
	if request.method == 'POST':
		scoresdataform = ScoresForm(data=request.POST)
	else:
		scoresdataform = ScoresForm()

	return render(request, 'registration/scores_data.html', {'scoresdataform': scoresdataform})

@login_required
def academicdata(request):
	if request.method == 'POST':
		academicdataform = AcademicsForm(data=request.POST)
	else:
		academicdataform = Stage4Form()

	return render(request, 'registration/academic_data.html', {'academicdataform': academicdataform})