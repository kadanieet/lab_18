from students.models import Student, Score
from django import forms
from django.conf import settings


class BioForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('Surname', 'Middle_Name', 'First_Name', 'Birth_Certificate_Number', 'Gender', 'Level', 'location')


class SocialFormP(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('Date_of_Birth', 'Class', 'School', 'Religion', 'Hobbies', 'Profile_Picture')


class SocialFormH(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('Date_of_Birth', 'Form', 'School', 'Religion', 'Hobbies', 'Profile_Picture')


class LessonForm(forms.ModelForm):
    lesson_times = forms.MultipleChoiceField(label='Lesson Times', choices=settings.LESSON_TIMES,
                                             widget=forms.CheckboxSelectMultiple,
                                             help_text='Select the times when you would like the student to be tutored')
    no_of_lessons = forms.IntegerField(label='Number of Lessons per Week', min_value=1, max_value=21, initial=1,
                                       required=True, help_text='Enter the number of lessons you would like to be delivered to the student per week')

    class Meta:
        model = Student
        fields = ['no_of_lessons',]


class SubjectsForm(forms.ModelForm):
    subjects = forms.MultipleChoiceField(label='Interested Subjects',
                                         help_text='Select the subjects you are interested in tutoring')
