# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from clients.models import Client
from django.conf import settings
from rabbitutors.validators import validate_scores


class Student(models.Model):
    student_id = models.AutoField(primary_key=True, null=False)
    surname = models.CharField(name='Surname', max_length=10, default=None)
    middle_name = models.CharField(name='Middle_Name', max_length=10, null=True)
    first_name = models.CharField(name='First_Name', max_length=10, default=None)
    birth_no = models.IntegerField(name='Birth_Certificate_Number', default=None, help_text='The Student Birth Certificate Number', unique=True)
    gender = models.CharField(name='Gender', max_length=1, choices=settings.GENDER_CHOICES, default=None, blank=True)
    location = models.BooleanField(help_text='Your registered location will be used as the default for the student you are registering. Check for YES or leave unchecked for NO')
    religion = models.CharField(name='Religion', max_length=2, choices=settings.RELIGION_CHOICES, default=None, blank=True)
    dob = models.DateField(name='Date_of_Birth', editable=True, blank=False, help_text='Format:MM/DD/YYYY')
    level = models.CharField(name='Level', max_length=1, default=None, blank=True, choices=settings.LEVEL_CHOICES)
    Class = models.CharField(name='Class', blank=True, default=None, max_length=1, null=True, choices=settings.CLASS_CHOICES, help_text='Which Class is the student Currently in?')
    Form = models.CharField(name='Form', blank=True, default=None, max_length=1, null=True, choices=settings.FORM_CHOICES, help_text='Which Form is the student Currently in?')
    school = models.CharField(name='School', max_length=30, null=False)
    hobbies = models.TextField(name='Hobbies', max_length=160, default=None)
    longitude = models.FloatField()
    latitude = models.FloatField()
    has_scores = models.BooleanField(default=False)
    has_subjects = models.BooleanField(default=False)
    subjects = models.TextField(name='Subjects', max_length=160, default=None, null=True)
    picture = models.ImageField(name='Profile_Picture', upload_to='profile_images', null=True)
    client = models.OneToOneField(Client, default=None, on_delete=models.CASCADE)
    has_lesson_times = models.BooleanField(default=False)
    lesson_times = models.CharField(max_length=5, null=True, default=None)
    no_of_lessons = models.PositiveSmallIntegerField(null=True, default=0, verbose_name='Number of Lessons per Week',
                                                     help_text='Enter the number of lessons you would like to be delivered to the student per week')


class Score(models.Model):
    scores_id = models.AutoField(primary_key=True, null=False)
    avg_score = models.PositiveSmallIntegerField(name='Mean_Score', null=True, default=0)
    mathematics = models.PositiveSmallIntegerField(name='Mathematics', default=0, null=True, validators=[validate_scores])
    english = models.PositiveSmallIntegerField(name='English', default=0, null=True, validators=[validate_scores])
    kiswahili = models.PositiveSmallIntegerField(name='Kiswahili', default=0, null=True, validators=[validate_scores])
    cre = models.PositiveSmallIntegerField(name='CRE', default=0, null=True, validators=[validate_scores])
    ire = models.PositiveSmallIntegerField(name='IRE', default=0, null=True, validators=[validate_scores])
    hre = models.PositiveSmallIntegerField(name='HRE', default=0, null=True, validators=[validate_scores])
    science = models.PositiveSmallIntegerField(name='Science', default=0, null=True, validators=[validate_scores])
    social_studies = models.PositiveSmallIntegerField(name='Social_Studies', default=0, null=True, validators=[validate_scores])
    french = models.PositiveSmallIntegerField(name='French', default=0, null=True, validators=[validate_scores])
    german = models.PositiveSmallIntegerField(name='German', default=0, null=True, validators=[validate_scores])
    spanish = models.PositiveSmallIntegerField(name='Spanish', default=0, null=True, validators=[validate_scores])
    chemistry = models.PositiveSmallIntegerField(name='Chemistry', default=0, null=True, validators=[validate_scores])
    biology = models.PositiveSmallIntegerField(name='Biology', default=0, null=True, validators=[validate_scores])
    physics = models.PositiveSmallIntegerField(name='Physics', default=0, null=True, validators=[validate_scores])
    agriculture = models.PositiveSmallIntegerField(name='Agriculture', default=0, null=True, validators=[validate_scores])
    geography = models.PositiveSmallIntegerField(name='Geography', default=0, null=True, validators=[validate_scores])
    history = models.PositiveSmallIntegerField(name='History', default=0, null=True, validators=[validate_scores])
    business_studies = models.PositiveSmallIntegerField(name='Business_Studies', default=0, null=True, validators=[validate_scores])
    music = models.PositiveSmallIntegerField(name='Music', default=0, null=True, validators=[validate_scores])
    home_science = models.PositiveSmallIntegerField(name='Home_Science', default=0, null=True, validators=[validate_scores])
    art_and_craft = models.PositiveSmallIntegerField(name='Art_and_Craft', default=0, null=True, validators=[validate_scores])
    student = models.OneToOneField(Student, default=1, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return  self.scores_id

    class Meta:
        verbose_name_plural = 'Subjects'
