from django.conf.urls import url, include
from students import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'students'

urlpatterns = [
    url(r'^view-students/$', views.view_students, name='view_students'),
    url(r'^view-profile/$', views.profile, name='viewprofile'),
    url(r'^bio-data/$', views.biodata, name='biodata'),
    url(r'^location/$', views.location, name='location'),
    url(r'^social-data/$', views.socialdata, name='socialdata'),
    url(r'^subjects/$', views.subjects, name='subjects'),
    url(r'^scores-data/$', views.scores, name='scores'),
    url(r'^lessons-data/$', views.lessons, name='lessons'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)