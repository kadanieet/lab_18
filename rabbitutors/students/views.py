# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from rabbitutors.forms import SubjectsForm, PriSubjectsForm
from django.contrib.auth.decorators import login_required
from .forms import SocialFormH, SocialFormP, BioForm, LessonForm
from .models import Student, Score
from clients.models import Client
from rabbitutors.utils import save_data_to_model, create_fields_list, clear_session_data, list_to_string, clear_unicode_string
from django.conf import settings
from django.forms import modelformset_factory, modelform_factory
from rabbitutors.models import PrimarySubjects, SecondarySubjects, RabbiUser
from tutors.models import Tutor
from classes.models import Assigned


@login_required
def view_students(request):
    context_dict = {}
    context_dict.update(get_students(request))

    return render(request, context_dict['template'], context_dict)


"""------------------------------------------PROFILE VIEWS---------------------------------------"""


@login_required
def profile(request):
    context_dict = {}
    if request.method == 'POST':
        student_profile = Student.objects.get(student_id=request.POST['student_id'])

        if request.POST['app'] == 'tutors':
            context_dict['template'] = 'tutors/student_profile_view.html'

            # Fetch the client profile
            client_profile = Client.objects.get(Client_ID=student_profile.client_id)
            context_dict['client_name'] = client_profile.First_Name + ' ' + client_profile.Middle_Name + ' ' + client_profile.Surname
            context_dict['client_relg'] = settings.RELIGIONS[client_profile.Religion]
            context_dict['client_gender'] = settings.GENDERS[client_profile.Gender]
            context_dict['client_phone'] = client_profile.Phone_Number
            context_dict['client_email'] = RabbiUser.objects.get(id=int(clear_unicode_string(client_profile.Client_ID)[1:])).email
            context_dict['client_occup'] = client_profile.Occupation

        elif request.POST['app'] == 'clients':
            context_dict['template'] = 'clients/student_profile_view.html'
            context_dict['dob'] = student_profile.Date_of_Birth

        # Common profile data for a student
        context_dict['name'] = student_profile.First_Name + ' ' + student_profile.Middle_Name + ' ' + student_profile.Surname
        context_dict['school'] = student_profile.School
        context_dict['class'] = student_profile.Class
        context_dict['relg'] = settings.RELIGIONS[student_profile.Religion]
        context_dict['gender'] = settings.GENDERS[student_profile.Gender]

    return render(request, context_dict['template'], context_dict)


"""------------------------------------------REGISTRATION VIEWS---------------------------------------"""


@login_required
def biodata(request):
    if request.method == 'POST':
        biodataform = BioForm(data=request.POST)
        if biodataform.is_valid():
            request.session['s_biodata'] = biodataform.cleaned_data
            if biodataform.cleaned_data['location']:
                return redirect('students:socialdata')
            else:
                return redirect('students:location')
        else:
            print(biodataform.errors)
    else:
        biodataform = BioForm()

    return render(request, 'registration/bio_data.html', {'biodataform': biodataform})


@login_required
def location(request):
    if request.method == 'POST':
        lon = request.POST.get('long')
        lat = request.POST.get('lat')
        # We need to check the credibility of the values before submit
        if (lat > -90 or lat < 90) and (lon > -90 or lon < 90):
            bio_data = request.session.get('s_biodata')
            bio_data.update({'latitude': lat, 'longitude': lon})
            request.session['s_biodata'] = bio_data
            return redirect('students:socialdata')
        else:
            pass
    else:
        lon = Client.objects.get(Client_ID=request.session['client_id']).longitude
        lat = Client.objects.get(Client_ID=request.session['client_id']).latitude
        bio_data = request.session.get('s_biodata')
        bio_data.update({'latitude': lat, 'longitude': lon})
        request.session['s_biodata'] = bio_data
        print(bio_data)
        return redirect('students:socialdata')

    return render(request, 'registration/location.html')


@login_required
def socialdata(request):
    level = request.session.get('s_biodata')['Level']
    request.session['level'] = level
    if request.method == 'POST':
        if level == 'P':
            socialform = SocialFormP(data=request.POST, files=request.FILES)
        elif level == 'S':
            socialform = SocialFormH(data=request.POST, files=request.FILES)

        if socialform.is_valid():
            bio_data = request.session.get('s_biodata')
            bio_data.update(socialform.cleaned_data)
            bio_data.update({'client_id': request.session.get('client_id')})
            print(bio_data)
            if save_data_to_model(Student, bio_data):
                if clear_session_data(request, 's_biodata'):
                    return redirect('clients:students')
    else:
        if level == 'P':
            socialform = SocialFormP()
        elif level == 'S':
            socialform = SocialFormH()

    return render(request, 'registration/pro_social_data.html', {'prosocialform': socialform})


@login_required
def subjects(request):
    level = None
    relg = None
    if 'level' in request.session:
        level = 'P'
    elif 'id' in request.GET:
        request.session['student_id'] = request.GET['id']
        level = Student.objects.get(student_id=request.GET['id']).Level
        relg = settings.RELIGIONS[Student.objects.get(student_id=request.GET['id']).Religion]

    if request.method == 'POST':
        if level == 'P':
            PriSubjectsForm = modelform_factory(PrimarySubjects, exclude=check_religion(relg))
            subjectsform = PriSubjectsForm(data=request.POST)

        elif level == 'S':
            SubjectsForm = modelform_factory(SecondarySubjects, exclude=check_religion(relg))
            subjectsform = SubjectsForm(data=request.POST)

        if subjectsform.is_valid():
            request.session['s_fields'] = create_fields_list(subjectsform.cleaned_data)
            Student.objects.update_or_create(student_id=request.GET['id'], Level=level, defaults={'Subjects':list_to_string(request.session['s_fields'])})
            Student.objects.update_or_create(student_id=request.GET['id'], Level=level, defaults={'has_subjects': True})
            return redirect('students:scores')
        else:
            print(subjectsform.errors)

    else:
        if level == 'P':
            PriSubjectsForm = modelform_factory(PrimarySubjects, exclude=check_religion(relg))
            subjectsform = PriSubjectsForm()
        elif level == 'S':
            SubjectsForm = modelform_factory(SecondarySubjects, exclude=check_religion(relg))
            subjectsform = SubjectsForm()

    return render(request, 'registration/subject.html', {'subjectsform': subjectsform})


@login_required
def scores(request):
    fields = request.session.get('s_fields')
    ScoresForm = modelform_factory(Score, fields=fields)

    if request.method == 'POST':
        scoresform = ScoresForm(data=request.POST)
        if scoresform.is_valid():
            scores_data = scoresform.cleaned_data
            scores_data.update({'student_id':request.session['student_id']})
            if save_data_to_model(Score, scores_data):
                return redirect('students:lessons')
    else:
        scoresform = ScoresForm()

    return render(request, 'registration/scores.html', {'scoresform': scoresform})


@login_required
def lessons(request):
    if request.method == 'POST':
        lessonsform = LessonForm(data=request.POST)
        if lessonsform.is_valid():
            lessons_data = lessonsform.cleaned_data
            Student.objects.update_or_create(student_id=request.session['student_id'],
                                             defaults={'lesson_times': list_to_string(lessons_data['lesson_times']),
                                                          'no_of_lessons': lessons_data['no_of_lessons'],
                                                       'has_lesson_times': True})
            #Student.objects.update_or_create(student_id=request.session['student_id'], Level=request.session['level'], defaults={'lesson_times': list_to_string(lessons_data['lesson_times'])})
            #Student.objects.update_or_create(student_id=request.session['student_id'], Level=request.session['level'], defaults={'no_of_lessons': lessons_data['no_of_lessons']})
            #Student.objects.update_or_create(student_id=request.session['student_id'], Level=request.session['level'], defaults={'has_lesson_times': True})

            clear_session_data(request, ['student_id','level'])
            return redirect('clients:portal')
        else:
            request.session['student_id'] = request.POST['student_id']
            request.session['level'] = request.POST['level']
            print(lessonsform.errors)
    else:
        lessonsform = LessonForm()

    return render(request, 'registration/lessons.html', {'lessonsform':lessonsform})


"""------------------------------------------UTILITY FUNCTIONS---------------------------------------"""


def check_religion(relgion):
    if relgion == 'Christian':
        return ('IRE', 'HRE')
    elif relgion == 'Hindu':
        return ('IRE', 'CRE')
    elif relgion == 'Muslim':
       return ('CRE', 'HRE')
    else:
        return ()


def get_students(request):
    infor_dict = {}
    students_dict = []
    if request.user.is_client:
        if Student.objects.filter(client_id=request.session['client_id']).exists():
            students_dict = Student.objects.filter(client_id=request.session['client_id']).all()
            infor_dict.update({'students': students_dict, 'template': 'clients/students.html'})
            return infor_dict
    elif request.user.is_tutor:
        if Assigned.objects.filter(tutor_id=request.session['tutor_id']).exists():
            for each_allocation in Assigned.objects.filter(tutor_id=request.session['tutor_id']):
                students_dict.append(Student.objects.filter(student_id=each_allocation.student_id))
            infor_dict.update({'students': students_dict, 'template': 'tutors/students.html'})
            return infor_dict

    return None
