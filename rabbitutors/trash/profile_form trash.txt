from allauth.account.forms import UserForm
from django.contrib.auth.forms import UserChangeForm

    def save(self, *args, **kwargs):
        self.user_form.save(*args, **kwargs)
        return super(EditProfileForm, self).save(*args, **kwargs)