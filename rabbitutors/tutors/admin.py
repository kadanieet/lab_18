# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from tutors.models import Tutor, Grades

# Register your models here.
admin.site.register(Tutor)
admin.site.register(Grades)