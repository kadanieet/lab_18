from django import forms
from tutors.models import Tutor, Grades


class BioForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('Surname', 'Middle_Name', 'First_Name', 'ID_Number', 'Phone_Number', 'has_tsc_no')


class ProSocialFormT(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('Gender', 'Occupation', 'Organization', 'Religion', 'Hobbies', 'TSC_Number')


class ProSocialForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('Gender', 'Occupation', 'Organization', 'Religion', 
        'Hobbies', 'Final_High_School_Attended', 'Final_Primary_School_Attended')


class MediaFormT(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('TSC_Certificate', 'Profile_Picture')


class MediaForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('KCSE_Slip', 'Profile_Picture')
        

class GradesForm(forms.ModelForm):
    class Meta:
        model = Grades
        exclude = ('grades_id', 'tutor',)


"""------------------------------------------FOR NON GOVERNMENT TEACHERS---------------------------------------"""


class EditUserForm(forms.ModelForm):
    class Meta:
        model = Tutor
        exclude = ('Tutor_ID', 'longitude', 'latitude', 'user', 'has_tsc_no', 'has_grades', 'Subjects', 'TSC_Number',
                   'TSC_Certificate', 'Profile_Picture', 'KCSE_Slip', 'students_assigned')


class EditProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)

        profile = kwargs.get('instance')
        if profile:
            kwargs['instance'] = profile

        self.user_form = EditUserForm(*args, **kwargs)

        self.fields.update(self.user_form.fields)
        self.initial.update(self.user_form.initial)

    class Meta:
        model = Tutor
        exclude = ('Tutor_ID', 'longitude', 'latitude', 'user', 'has_tsc_no', 'has_grades', 'Subjects', 'TSC_Number',
                   'TSC_Certificate', 'Profile_Picture', 'KCSE_Slip', 'students_assigned')


"""------------------------------------------FOR GOVERNMENT TEACHERS---------------------------------------"""


class EditUserFormT(forms.ModelForm):
    class Meta:
        model = Tutor
        exclude = ('Tutor_ID', 'longitude', 'latitude', 'user',
        'has_tsc_no', 'has_grades', 'Subjects',
        'Final_High_School_Attended', 'Final_Primary_School_Attended', 'students_assigned')


class EditProfileFormT(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditProfileFormT, self).__init__(*args, **kwargs)

        profile = kwargs.get('instance')
        if profile:
            kwargs['instance'] = profile

        self.user_form = EditUserFormT(*args, **kwargs)

        self.fields.update(self.user_form.fields)
        self.initial.update(self.user_form.initial)

    class Meta:
        model = Tutor
        exclude = ('Tutor_ID', 'longitude', 'latitude', 'user',
        'has_tsc_no', 'has_grades', 'Subjects',
        'Final_High_School_Attended', 'Final_Primary_School_Attended', 'students_assigned')

