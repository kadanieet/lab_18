# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from rabbitutors.validators import validate_points


class Tutor(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default='')
    tutor_id = models.CharField(name='Tutor_ID',max_length=4, unique=True, primary_key=True)
    id_no = models.IntegerField(name='ID_Number', default=None, help_text='Your National Identification Number', unique=True)
    surname = models.CharField(name='Surname', max_length=10)
    middle_name = models.CharField(name='Middle_Name', max_length=10, null=True)
    first_name = models.CharField(name='First_Name', max_length=10)
    gender = models.CharField(name='Gender', max_length=1, choices=settings.GENDER_CHOICES, default=None, blank=True)
    religion = models.CharField(name='Religion', max_length=2, choices=settings.RELIGION_CHOICES, default=None, blank=True)
    occupation = models.CharField(name='Occupation', max_length=20, null=True)
    organization = models.CharField(name='Organization', max_length=30, null=True, help_text='Enter the organization or institution where you practice your occupation. If your are a teacher, please enter the school you teach')
    has_tsc_no = models.BooleanField(default=False, verbose_name='TSC Number', help_text='Select if you have a TSC Number or otherwise leave unselected')
    high_school = models.CharField(name='Final_High_School_Attended', null=True, max_length=30, help_text='The High School or Secondary School in which you did your final exam')
    primary_school = models.CharField(name='Final_Primary_School_Attended', null=True, max_length=30, help_text='The Primary School in which you did your final exam')
    hobbies = models.TextField(name='Hobbies', max_length=160, help_text='Enter Your Personal Hobbies', null=True)
    tsc_no = models.CharField(name='TSC_Number', max_length=8, null=True, help_text='Enter your TSC Number if you are a Government Employed Teacher', unique=True)
    phone = models.CharField(name='Phone_Number', max_length=10)
    longitude = models.FloatField()
    latitude = models.FloatField()
    kcse_slip = models.FileField(name='KCSE_Slip',upload_to='kcse_slips', null=True, help_text='Upload a scanned document of your KCSE Slip or Certificate')
    tsc_cert = models.FileField(name='TSC_Certificate', upload_to='tsc_certs', null=True, help_text='Upload a scanned document of your TSC Certificate')
    picture = models.ImageField(name='Profile_Picture', upload_to='profile_images', null=True)
    subjects = models.TextField(name='Subjects', max_length=160, default=None, null=True)
    has_grades = models.BooleanField(default=False)
    teacher_level = models.CharField(verbose_name='Level of Teaching', max_length=1,
                                     help_text='Which level do you teach or would you like to teach?', null=True,
                                     default=None, choices=settings.TEACHING_LEVEL_CHOICES, blank=True)
    students_assigned = models.PositiveSmallIntegerField(default=0, null=False)


class Grades(models.Model):
    grades_id = models.AutoField(primary_key=True, null=False)
    avg_grade = models.CharField(max_length=2, verbose_name='Average Grade', null=False, choices=settings.GRADE_CHOICES)
    avg_points = models.SmallIntegerField(null=False, verbose_name='Average Points', blank=True, validators=[validate_points])
    mathematics = models.CharField(max_length=2, name='Mathematics', default='N', choices=settings.GRADE_CHOICES, blank=True)
    english = models.CharField(max_length=2, name='English', default='N', choices=settings.GRADE_CHOICES, blank=True)
    kiswahili = models.CharField(max_length=2, name='Kiswahili', default='N', choices=settings.GRADE_CHOICES, blank=True)
    cre = models.CharField(max_length=2, name='CRE', default='N', choices=settings.GRADE_CHOICES, blank=True)
    ire = models.CharField(max_length=2, name='IRE', default='N', choices=settings.GRADE_CHOICES, blank=True)
    hre = models.CharField(max_length=2, name='HRE', default='N', choices=settings.GRADE_CHOICES, blank=True)
    french = models.CharField(max_length=2, name='French', default='N', choices=settings.GRADE_CHOICES, blank=True)
    german = models.CharField(max_length=2, name='German', default='N', choices=settings.GRADE_CHOICES, blank=True)
    spanish = models.CharField(max_length=2, name='Spanish', default='N', choices=settings.GRADE_CHOICES, blank=True)
    chemistry = models.CharField(max_length=2, name='Chemistry', default='N', choices=settings.GRADE_CHOICES, blank=True)
    biology = models.CharField(max_length=2, name='Biology', default='N', choices=settings.GRADE_CHOICES, blank=True)
    physics = models.CharField(max_length=2, name='Physics', default='N', choices=settings.GRADE_CHOICES, blank=True)
    agriculture = models.CharField(max_length=2, name='Agriculture', default='N', choices=settings.GRADE_CHOICES, blank=True)
    geography = models.CharField(max_length=2, name='Geography', default='N', choices=settings.GRADE_CHOICES, blank=True)
    history = models.CharField(max_length=2, name='History', default='N', choices=settings.GRADE_CHOICES, blank=True)
    business_studies = models.CharField(max_length=2, name='Business_Studies', default='N', choices=settings.GRADE_CHOICES, blank=True)
    music = models.CharField(max_length=2, name='Music', default='N', choices=settings.GRADE_CHOICES, blank=True)
    home_science = models.CharField(max_length=2, name='Home_Science', default='N', choices=settings.GRADE_CHOICES, blank=True)
    art_and_craft = models.CharField(max_length=2, name='Art_and_Craft', default='N', choices=settings.GRADE_CHOICES, blank=True)
    tutor = models.OneToOneField(Tutor, default='null', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Grades'
