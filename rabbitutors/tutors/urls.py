from django.conf.urls import url, include
from tutors import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'tutors'

urlpatterns = [
    url(r'^payment/$', views.payment, name='payment'),
    url(r'^portal/$', views.portal, name='portal'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^students/$', views.students, name='students'),
    url(r'^classes/$', views.classes, name='classes'),
    url(r'^location/$', views.location, name='location'),
    url(r'^bio-data/$', views.biodata, name='biodata'),
    url(r'^pro-social-data/$', views.prosocialdata, name='prosocialdata'),
    url(r'^media-data/$', views.mediadata, name='mediadata'),
    url(r'^grades-data/$', views.gradesdata, name='gradesdata'),
    url(r'^subjects/$', views.subjects, name='subjects'),
    url(r'^edit-profile/', views.edit_profile, name='editprofile'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)