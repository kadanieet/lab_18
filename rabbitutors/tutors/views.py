# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from tutors.models import Tutor
from django.contrib.auth.decorators import login_required, user_passes_test
from tutors.forms import BioForm, ProSocialForm, ProSocialFormT, MediaForm, MediaFormT, EditProfileForm#, EditProfileFormT
from rabbitutors.forms import SubjectsForm
from django.conf import settings
from django.forms import modelform_factory
from tutors.models import Grades
from rabbitutors.utils import save_data_to_model, set_profile, clear_session_data, create_fields_list, \
	clear_unicode_string, create_changed_data_dict, clear_unicode_list, replace_underscore
from students.models import Student
from classes.models import Assigned
from django.contrib.auth.hashers import check_password
from rabbitutors.models import RabbiUser


def tutor_check(user):
	return user.is_tutor


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def portal(request):
	context_dict = {}
	request.session['tutor_id'] = 'T' + str(request.user.id)

	if request.user.has_profile:
		if has_tsc_no(request):
			pass
		else:
			if has_grades(request):
				pass
			else:
				context_dict['no_grades'] = True
	else:
		context_dict['no_profile'] = True

	return render(request, 'tutors/portal.html', context_dict)


@login_required
def profile(request):
	context_dict = {}

	if request.method == 'POST':
		context_dict['template'] = 'clients/tutors_profile.html'

		tutor_profile = Tutor.objects.get(Tutor_ID=request.POST['tutor_id'])
		context_dict['name'] = tutor_profile.First_Name + ' ' + tutor_profile.Middle_Name + ' ' + tutor_profile.Surname
		context_dict['relg'] = settings.RELIGIONS[tutor_profile.Religion]
		context_dict['phone'] = tutor_profile.Phone_Number
		context_dict['gender'] = settings.GENDERS[tutor_profile.Gender]
		context_dict['email'] = RabbiUser.objects.get(id=int(request.POST['tutor_id'][1:])).email

	else:
		context_dict['template'] = 'tutors/profile.html'

		if request.user.has_profile:
			context_dict['profile'] = True

			tutor_profile = Tutor.objects.get(Tutor_ID=request.session['tutor_id'])

			if 'updated' in request.session:
				context_dict['updated'] = request.session['updated']

			context_dict['name'] = tutor_profile.First_Name + ' ' + tutor_profile.Middle_Name + ' ' + tutor_profile.Surname
			context_dict['idno'] = tutor_profile.ID_Number
			if tutor_profile.has_tsc_no:
				context_dict['tsc'] = True
				context_dict['tscno'] = tutor_profile.TSC_Number
				context_dict['tsc_cert'] = 'Not Changed'
			else:
				context_dict['tsc'] = False
				context_dict['pri'] = tutor_profile.Final_Primary_School_Attended
				context_dict['sec'] = tutor_profile.Final_High_School_Attended
				context_dict['kcse_cert'] = 'Not Changed'
			context_dict['subjs'] = clear_unicode_string(tutor_profile.Subjects).replace('_', ' ').split(',', tutor_profile.Subjects.count(','))
			context_dict['gender'] = settings.GENDERS[tutor_profile.Gender]
			context_dict['occ'] = tutor_profile.Occupation
			context_dict['org'] = tutor_profile.Organization
			context_dict['phone'] = tutor_profile.Phone_Number
			context_dict['hobbies'] = tutor_profile.Hobbies
			context_dict['relg'] = settings.RELIGIONS[tutor_profile.Religion]
			context_dict['email'] = request.user.email
		else:
			context_dict['profile'] = False

	return render(request, context_dict['template'], context_dict)


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def edit_profile(request):
	context_dict = {}
	if request.method == 'POST':
		edit_profile_form = EditProfileForm(data=request.POST, instance=Tutor.objects.get(Tutor_ID=request.session['tutor_id']))

		if 'Cancel' in request.POST:
			return redirect('tutors:profile')

		elif 'Update' in request.POST:

			if edit_profile_form.has_changed():

				if edit_profile_form.is_valid():
					data_dict = create_changed_data_dict(edit_profile_form)

					if check_password(request.POST['password'], request.user.password):
						Tutor.objects.update_or_create(Tutor_ID=request.session.get('tutor_id'), defaults=data_dict)
						request.session['updated'] = replace_underscore(data_dict.keys())
						return redirect('tutors:profile')
				else:
					print(edit_profile_form.errors)
			else:
				return redirect('tutors:profile')
	else:
		edit_profile_form = EditProfileForm(instance=Tutor.objects.get(Tutor_ID=request.session['tutor_id']))

	context_dict['edit_profile_form'] = edit_profile_form
	
	return render(request, 'tutors/profile_edit.html', context_dict)


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def students(request):
	context_dict = {}
	students_ids = []
	students = []

	if Assigned.objects.filter(tutor=request.session['tutor_id']).exists():
		for each_assigment in Assigned.objects.filter(tutor=request.session['tutor_id']).all():
			students_ids.append(each_assigment.student)

		students_ids = list(set(students_ids))

		for student in students_ids:
			students.append(Student.objects.get(student_id=student))

		context_dict['students'] = students
	else:
		context_dict['students'] = None
	
	return render(request, 'tutors/students.html', context_dict)


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def classes(request):
	context_dict = {}
	return render(request, 'tutors/classes.html', context_dict)


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def payment(request):
	return render(request, 'tutors/payment.html')


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def biodata(request):
	if request.method == 'POST':
		biodataform = BioForm(data=request.POST)
		if biodataform.is_valid():
			request.session['tbiodata'] = biodataform.cleaned_data
			return redirect('tutors:location')
		else:
			print(biodataform.errors)
	else:
		biodataform = BioForm()

	return render(request, 'registration/bio_data.html', {'biodataform': biodataform})


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def location(request):
	if request.method == 'POST':
		long = request.POST.get('long')
		lat = request.POST.get('lat')
		# We need to check the credibility of the values before submit
		if (lat > -90 or lat < 90) and (long > -90 or long < 90):
			bio_data = request.session.get('tbiodata')
			bio_data.update({'latitude':lat, 'longitude':long})
			request.session['tbiodata'] = bio_data
			return  redirect('tutors:prosocialdata')

	return render(request, 'registration/location.html')


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def prosocialdata(request):
	has_tsc_no = request.session.get('tbiodata')['has_tsc_no']
	if request.method == 'POST':

		if has_tsc_no:
			prosocialform = ProSocialFormT(data=request.POST)
		else:
			prosocialform = ProSocialForm(data=request.POST)

		if prosocialform.is_valid():
			bio_data = request.session.get('tbiodata')
			bio_data.update(prosocialform.cleaned_data)
			request.session['tbiodata'] = bio_data
			return redirect('tutors:mediadata')
		else:
			print(prosocialform.errors)
	else:
		if has_tsc_no:
			prosocialform = ProSocialFormT()
		else:
			prosocialform = ProSocialForm()

	return render(request, 'registration/pro_social_data.html', {'prosocialform': prosocialform})


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def mediadata(request):
	has_tsc = request.session.get('tbiodata')['has_tsc_no']
	if request.method == 'POST':
		if has_tsc:
			mediaform = MediaFormT(data=request.POST, files=request.FILES)
		else:
			mediaform = MediaForm(data=request.POST, files=request.FILES)

		if mediaform.is_valid():
			bio_data = request.session.get('tbiodata')
			if has_tsc:
				bio_data.update({'Profile_Picture' : request.FILES['Profile_Picture'], 'TSC_Certificate' : request.FILES['TSC_Certificate']})
				url = 'tutors:subjects'
			else:
				bio_data.update({'Profile_Picture': request.FILES['Profile_Picture'],'KCSE_Slip': request.FILES['KCSE_Slip']})
				url = 'tutors:subjects'

			bio_data.update({'Tutor_ID' : request.session.get('tutor_id'), 'user_id' : request.user.id})
			if save_data_to_model(Tutor, bio_data):
				if set_profile(request):
					if clear_session_data(request, 'tbiodata'):
						return redirect(url)
	else:
		if has_tsc:
			mediaform = MediaFormT()
		else:
			mediaform = MediaForm()

	return	render(request, 'registration/media_data.html', {'mediaform': mediaform})


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def subjects(request):
	if has_grades(request):
		return redirect('tutors:portal')
	else:
		if request.method == 'POST':
			subjectsform = SubjectsForm(data=request.POST)
			if subjectsform.is_valid():
				request.session['tfields'] = create_fields_list(subjectsform.cleaned_data)
				return redirect('tutors:gradesdata')
			else:
				print(subjectsform.errors)
		else:
			subjectsform = SubjectsForm()

	return render(request, 'tutors/subject.html', {'subjectsform': subjectsform})


@login_required
@user_passes_test(tutor_check, login_url=settings.RESTRICTED_URL)
def gradesdata(request):
	if has_grades(request):
		return redirect('tutors:portal')
	else:
		fields = request.session['tfields']
		fields.insert(0, 'avg_grade')
		fields.insert(1, 'avg_points')
		GradesForm = modelform_factory(Grades, fields=fields)
		if request.method == 'POST':
			gradesdataform = GradesForm(data=request.POST)
			if gradesdataform.is_valid():
				grades_data = gradesdataform.cleaned_data
				grades_data.update({'tutor_id': request.session.get('tutor_id')})
				subjs = request.session['tfields']
				subjs.pop(0)
				subjs.pop(0)
				print(grades_data)
				#if save_grades(grades_data):
					#Tutor.objects.update_or_create(Tutor_ID=request.session.get('tutor_id'),
												   #defaults={'Subjects':list_to_string(subjs), 'has_grades':True})

					#Tutor.objects.filter(Tutor_ID=request.session.get('tutor_id')).update(has_grades=)
					#Tutor.objects.filter(Tutor_ID=request.session.get('tutor_id')).update(Subjects=list_to_string(subjs))
					#return redirect('tutors:portal')
			else:
				print(gradesdataform.errors)
		else:
			gradesdataform = GradesForm()

	return render(request, 'tutors/grades_data.html', {'gradesdataform': gradesdataform})


def save_grades(grades_dict):
	grades = Grades.objects.create(**grades_dict)
	grades.save()
	return True


def has_grades(request):
	if Tutor.objects.get(Tutor_ID=request.session['tutor_id']).has_grades:
		return True


def has_tsc_no(request):
	if Tutor.objects.get(Tutor_ID=request.session['tutor_id']).has_tsc_no:
		return True


def evaluate_grades(grades_dict):
	pass